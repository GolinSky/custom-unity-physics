﻿using System.Collections;
using UnityEngine;

public class EnemyAnimManager : MonoBehaviour
{
    Animation anim;
    private void Awake()
    {
        anim = GetComponentInChildren<Animation>();
    }
    public void Fall()
    {
        Play("fall");
    }
    void Play(string name)
    {
        anim.Play(name);
    }
    IEnumerator checkFallCoroutine(string name)
    {
        yield return new WaitWhile(() => anim[name].enabled);
        Play("fallIdle");
    }
}
