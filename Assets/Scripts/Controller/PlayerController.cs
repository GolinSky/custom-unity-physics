﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace CustomPhysics
{
    public class PlayerController : MonoBehaviour
    {
        public static PlayerController Instance;
        CustomCharacterController characterController;
        float angle = 15;
        public Transform target;
        Transform gun;
        Transform spawnPoint;
        float dist;
        float speed = 1;
        Coroutine moveCor;
        private void Awake()
        {
            Instance = this;
            gun = transform.Find("gun");
        }
        void Start()
        {
            characterController = GetComponent<CustomCharacterController>();
            spawnPoint = gun.Find("spawnPoint");
            moveCor = StartCoroutine(MoveCoroutine());
        }
       
        IEnumerator MoveCoroutine()
        {
            while(true)
            {
                yield return new WaitForFixedUpdate();
                characterController.Move(speed);
            }
        }
        public void Shoot(Vector3 targetPos)
        {
            PoolManager.Instance.CreateProjectile(spawnPoint.position).AddForce(targetPos, angle);
            AudioManager.Instance.PlayShot();
        }
        public void SetAngle(float angle)
        {
            gun.localEulerAngles = Vector3.forward * angle;
            this.angle = angle;
        }
        public void SetDistance(float dist)
        {
            this.dist = dist;
        }
        public void SetSpeed(float speed)
        {
            this.speed = speed;
        }
        public void Stop()
        {
            StopCoroutine(moveCor);
            UI.Instance.EOF();
        }
    }
}

