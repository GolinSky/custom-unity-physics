﻿using UnityEngine;

namespace CustomPhysics
{
    public class AudioManager : MonoBehaviour
    {
        public AudioClip FonClip, ExpClip, GunClip;
        AudioSource fonSource, expSource, shotSource;
        public static AudioManager Instance;
      
        private void Awake()
        {
            Instance = this;
            InitSource(ref fonSource, FonClip);
            InitSource(ref expSource, ExpClip);
            InitSource(ref shotSource, GunClip);
            fonSource.loop = true;
            fonSource.Play();
        }
        void InitSource(ref AudioSource audioSource, AudioClip clip)
        {
            audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.clip = clip;
            audioSource.volume = 0.5f;
        }
        public void PlayExp()
        {
            expSource.Play();
        }
        public void PlayShot()
        {
            shotSource.Play();
        }
    }
}

