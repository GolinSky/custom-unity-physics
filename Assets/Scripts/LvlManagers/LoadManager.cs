﻿using UnityEngine;

namespace CustomPhysics
{
    public class LoadManager : MonoBehaviour
    {
        AsyncOperation _async;
        void Start()
        {
            _async = LvlManager.LoadLvlAsync(1);
            _async.allowSceneActivation = false;
        }

        void Update()
        {
            if (_async.progress == 0.9f)
            {
                _async.allowSceneActivation = true;
            }
        }
    }
}

