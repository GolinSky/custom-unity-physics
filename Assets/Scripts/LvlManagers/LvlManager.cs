﻿using UnityEngine;
using UnityEngine.SceneManagement;
namespace CustomPhysics
{
    public class LvlManager : MonoBehaviour
    {
        public static void RestartLvl()
        {
            loadLvl(0);
        }
        public static void LoadLvl(int index)
        {
            loadLvl(index);
        }
        public static AsyncOperation LoadLvlAsync(int index)
        {
            return SceneManager.LoadSceneAsync(index);
        }
        static void loadLvl(int index)
        {
            SceneManager.LoadSceneAsync(index);
        }
    }
}

