﻿using System.Collections;
using UnityEngine;
namespace CustomPhysics
{

    public class PoolManager : MonoBehaviour
    {
        public static PoolManager Instance;
        const int size = 30;
        Transform[] exp;
        Transform[] projectile;
        Transform[] expWave;
        Transform pool; // parent obj for spawn objs (exp)
        
        int currI_Exp = 0; // current index of massive's exp;
        int currI_Proj = 0; // curr index of massive's missiles;
        int currI_Wave = 0; // curr index of mass's exp waves

        private void Awake()
        {
            Instance = this;
        }

        private IEnumerator Start()
        {
            exp = new Transform[size];
            projectile = new Transform[size]; 
            expWave = new Transform[size];
            pool = CreatePool();
            
            Transform _spawnPool = CreatePool("Missile pool");
            Transform _spawnObject = ResManager.Instance.Projectile;

            for (int i = 0; i < projectile.Length; i++)
            {
                yield return new WaitForEndOfFrame();
                projectile[i] = Instantiate(_spawnObject, _spawnPool);
            }

            _spawnObject = ResManager.Instance.Exp;
            _spawnPool = CreatePool("Explosion pool");
            for (int i = 0; i < exp.Length; i++)
            {
                yield return new WaitForEndOfFrame();
                exp[i] = Instantiate(_spawnObject, _spawnPool);
            }
            _spawnObject = ResManager.Instance.ExpWave;
            _spawnPool = CreatePool("Explosion Wave pool");
            for (int i = 0; i < expWave.Length; i++)
            {
                yield return new WaitForEndOfFrame();
                expWave[i] = Instantiate(_spawnObject, _spawnPool);
            }
        }
        
        #region poolObjCreation
        Transform CreatePool()
        {
            Transform pool = new GameObject("Pool").transform;
            pool.SetParent(transform);
            return pool;
        }
        Transform CreatePool(string name)
        {
            Transform pool = new GameObject(name).transform;
            pool.SetParent(this.pool);
            return pool;

        }
        #endregion poolObjCreation
        #region GetObj
        public void CreateExp(Vector3 pos)
        {
            Create(exp[currI_Exp], ref currI_Exp).position = pos;
        }
        public ProjectileRigidBody CreateProjectile(Vector3 pos)
        {
            Transform buffMissile = Create(projectile[currI_Proj], ref currI_Proj) ;
            buffMissile.position = pos;
            return buffMissile.GetComponent<ProjectileRigidBody>();
        }
        public RigidBodyWave CreateWave(Vector3 pos)
        {
            Transform buffWave = Create(expWave[currI_Wave], ref currI_Wave);

            buffWave.position = pos;
            return buffWave.GetComponent<RigidBodyWave>();

        }
        Transform Create(Transform obj, ref int currI)
        {
            obj.gameObject.SetActive(true);
            currI += 1;
            currI = currI > size - 1 ? 0 : currI;
            return obj;
        }
        // TODO: ProjectileRigidBody component cached on  CreateProjectile method using delegate 

        #endregion GetObj
    }
}

