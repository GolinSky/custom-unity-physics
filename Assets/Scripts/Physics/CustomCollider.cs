﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
namespace CustomPhysics
{
    public class CustomCollider : MonoBehaviour
    {
        public List<Renderer> obj;
        Bounds[] bounds;
        void Start()
        {
            bounds = obj.Select(x => x.bounds).ToArray();
        }

        void FixedUpdate()
        {
            for (int i = 0; i < bounds.Length; i++)
                bounds[i] = obj[i].bounds;

            for (int i = 0; i < obj.Count; i++)
            {
                for (int j = 0; j < obj.Count; j++)
                {
                    if (i != j)
                    {
                        if (bounds[i].Intersects(bounds[j]))
                        {
                            print("Intersects");
                        }
                    }

                }
            }
        }
    }
}


