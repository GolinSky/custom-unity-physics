﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomPhysics
{
    public class CustomPhysicsManager : MonoBehaviour
    {
        public static CustomPhysicsManager Instance;
        private List<Renderer> primitives = new List<Renderer>();
        private List<Renderer> boundsFloor = new List<Renderer>();
        private List<Renderer> waves = new List<Renderer>();

        private List<BaseRigidBody> rigidBodies = new List<BaseRigidBody>();
        private List<RigidBodyWave> rigidWaves = new List<RigidBodyWave>();
        private Coroutine coroutine;

        private void Awake()
        {
            Instance = this;
        }
        public void Stop()
        {
            StopAllCoroutines();
            primitives = null;
            boundsFloor = null;
            waves = null;
            rigidBodies = null;
            rigidWaves = null;
            LvlManager.RestartLvl();

        }
        private void Start()
        {

            coroutine = StartCoroutine(Check());
        }

        IEnumerator Check()
        {
            while (true)
            {
                yield return new WaitForEndOfFrame();
                for (int i = 0; i < primitives.Count; i++)
                {
                    //CHECK COLL BETWEEN RIGIDBODIES & FLOORS
                    for (int j = 0; j < boundsFloor.Count; j++)
                    {
                        if (primitives[i].bounds.Intersects(boundsFloor[j].bounds))
                        {
                            rigidBodies[i].StopGravity();
                        }
                        else if (!primitives[i].bounds.Intersects(boundsFloor[j].bounds))
                        {
                            rigidBodies[i].StartGravity();
                        }
                    }
                    // CHECK COLL BETWEEN RIGIDBODIES
                    for (int j = i + 1; j < primitives.Count; j++)
                    {

                        if (primitives[i].bounds.Intersects(primitives[j].bounds))
                        {
                            Vector3 dirCollI = Vector3.zero;
                            Vector3 dirCollJ = Vector3.zero;

                            dirCollI = Vector3.Cross(rigidBodies[i].transform.right, rigidBodies[j].transform.position);
                            dirCollI = Vector3.Cross(rigidBodies[j].transform.right, rigidBodies[i].transform.position);
                            rigidBodies[i].CustomOnCollisionEnter(dirCollI);
                            rigidBodies[j].CustomOnCollisionEnter(dirCollJ);

                        }

                    }
                    for (int j = 0; j < waves.Count; j++)
                    {
                        if (primitives[i].bounds.Intersects(waves[j].bounds))
                        {
                            Vector3 vec = Vector3.Cross(rigidBodies[i].transform.right, waves[j].transform.position);
                            rigidBodies[i].OnWaveEnter(vec, rigidWaves[j].R);
                        }
                        if (waves[j].bounds.Intersects(primitives[i].bounds))
                        {
                            Vector3 vec = Vector3.Cross(rigidBodies[i].transform.right, waves[j].transform.position);
                            rigidBodies[i].OnWaveEnter(vec, rigidWaves[j].R);
                        }
                    }

                }
            }
        }

        #region addToCollection
        public void AddPrimitive(BaseRigidBody rigid)
        {
            rigidBodies.Add(rigid);
            primitives.Add(rigid.Render);
        }
        public void AddFloor(Renderer renderer)
        {
            boundsFloor.Add(renderer);
        }
        public void AddWave(RigidBodyWave rigid)
        {
            waves.Add(rigid.Render);
            rigidWaves.Add(rigid);

        }
        #endregion addToCollection
    }
}

#region boundsAngleColl
//Vector3 boundI = transform.TransformPoint(primitives[i].bounds.center);
//Vector3 boundJ = transform.TransformPoint(primitives[j].bounds.center);


//if (boundI.x > boundJ.x)
//{
//    dirCollI -= Vector3.right;
//    dirCollJ += Vector3.right;
//}
//else if (boundI.x < boundJ.x)
//{
//    dirCollI += Vector3.right;
//    dirCollJ -= Vector3.right;

//}
//if (boundI.y > boundJ.y)
//{
//    dirCollI += Vector3.up;
//}
//else if (boundJ.y > boundI.y)
//{
//    dirCollJ += Vector3.up;
//}
#endregion boundsAngleColl