﻿using UnityEngine;
using System.Collections;

namespace CustomPhysics
{
    public abstract class BaseRigidBody : MonoBehaviour
    {
        public float Force = 9.81f;
        public float Mass = 1;
        protected float gravity = 1;
        public Renderer Render { get; protected set; }

        protected virtual void Awake()
        {
            Render = GetComponent<Renderer>();
        }
        protected virtual void Start()
        {
            CustomPhysicsManager.Instance.AddPrimitive(this);
        }
        protected virtual IEnumerator gravityCoroutine()
        {
            Quaternion ident = transform.localRotation;
            ident.z = 0; 
            while (true)
            {
                yield return new WaitForFixedUpdate();
                ident = transform.localRotation;
                ident.z = 0;
                transform.position -= Vector3.up * Force * Mass * Time.fixedDeltaTime * gravity;
            }
        }
        public  virtual void AddForce(Vector3 dir, float power) { }
        public virtual void StopGravity() { }
        public virtual void StartGravity() { }
        public virtual void CustomOnCollisionEnter(Vector3 hitDir) { }
        public virtual void OnWaveEnter(Vector3 hitDir, float pow) { }
    }
}

