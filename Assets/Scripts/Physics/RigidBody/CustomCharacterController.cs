﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomPhysics
{
    public class CustomCharacterController : BaseRigidBody
    {

        protected override void Start()
        {
            base.Start();
            StartCoroutine(gravityCoroutine());
        }
        public void Move(float axisVal)
        {
            transform.position += Vector3.right * axisVal * Time.fixedDeltaTime * (gravity == 0?1f:0f);
        }
       
        #region changeState
        public override void StopGravity()
        {
            gravity = 0;
        }
        public override void StartGravity()
        {
            gravity = 1;
        }
        #endregion changeState
    }
}

