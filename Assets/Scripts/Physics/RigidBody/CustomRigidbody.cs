﻿using UnityEngine;
using System.Collections;
namespace CustomPhysics
{

    /// <summary>
    ///  custom rigidBody 4 primitives
    /// </summary>
    public class CustomRigidbody : BaseRigidBody
    {

        public bool UseGrav = true;

        delegate void del();
        del _disableInstr, _enableInsr;
        Coroutine cor;
        float velocity;
        float waveCounter = 10;

        protected override void Start()
        {
            base.Start();
            cor = StartCoroutine(addForce(Vector3.zero, 0f));
            if (UseGrav)
            {
                StartCoroutine(gravityCoroutine());
                gravity = 1;
                _disableInstr = new del(() => InitCollision());
                _enableInsr = new del(() => StartCoroutine(gravityCoroutine()));
            }
            else
            {
                _disableInstr = new del(() => { });
                _enableInsr = new del(() => { });
            }
            StartCoroutine(aligning());
        }
        void InitCollision()
        {
            gravity = 0;
            StopCoroutine(cor);
        }

        IEnumerator addForce(Vector3 dir, float power)
        {
            gravity = 0f;

            float angle = Vector3.Angle(transform.right, dir) * Mathf.Deg2Rad;

            Vector3 dir1 = Vector3.zero;
            float t = 0;
            dir.z = Random.Range(-1, 1);
            dir.z = Mathf.RoundToInt(dir.z);

            Vector3 target = transform.position + dir * power;

            float R = Vector3.Distance(target, transform.position) / 2;
            Vector3 startPos = transform.position;

            Vector3 dirRot = dir.x > 0 ? Vector3.forward : -Vector3.forward;

            target.y = 2;
            if (power != 0)
                while (t < 1f)
                {
                    yield return new WaitForFixedUpdate();
                    t += Time.fixedDeltaTime;

                    transform.localEulerAngles += dirRot * Mathf.Sin(angle) * Mathf.Rad2Deg * Time.fixedDeltaTime;
                    transform.position = MathParabola.Parabola(startPos, target, R, t);
                }
            yield return new WaitForFixedUpdate();
            gravity = 1;
            RecalculateBounds();
            _disableInstr = new del(() => InitCollision());
        }

        void RecalculateBounds() //TODO: CALL NOT ON UPDATE - BUT CALL IT MORE TIMES THAN NOW -- TRIGGER BUG -- MAY BE PLAYER BUG 
        {
            Vector3 pos = transform.position;
            Render.bounds.center.Set(pos.x, pos.y, pos.z);
        }
        IEnumerator aligning()
        {
            while (true)
            {
                yield return new WaitUntil(() => gravity == 0);
                Vector2 boundsCenter = transform.TransformPoint(Render.bounds.center);
                float angle = Vector2.Angle(boundsCenter, new Vector2(boundsCenter.x, 0));

                Vector3 circleCenter = new Vector3(transform.position.x, 0);

                Vector3 dirRot = transform.localEulerAngles.z > 180 ? -Vector3.forward : Vector3.forward;

                while (gravity == 0)
                {
                    yield return new WaitForFixedUpdate();
                    Vector2 boundsMax = Render.bounds.max;
                    Vector2 boundsMin = Render.bounds.min;

                    if (boundsMax.x - boundsMin.x == 1.0f)
                    {
                        break;
                    }
                    transform.RotateAround(circleCenter, dirRot, 1);
                    Vector3 vec = transform.localEulerAngles;
                    vec.z = Mathf.Round(vec.z);

                    transform.localEulerAngles = vec;
                }
                yield return new WaitUntil(() => gravity == 1);
            }
        }
        private void Update()
        {
            velocity += Mass * Force * gravity * Time.deltaTime;
            velocity = Mathf.Clamp(velocity, 0, 3f);
        }
        #region changeState
        public override void StopGravity()
        {
            _disableInstr();
        }
        public override void StartGravity()
        {
            gravity += Time.fixedDeltaTime;
            gravity = Mathf.Clamp(gravity, 0, 1);
        }
        #endregion changeState
        public override void AddForce(Vector3 dir, float power)
        {
            StopCoroutine(cor);
            cor = StartCoroutine(addForce(dir, power));
        }

        public override void CustomOnCollisionEnter(Vector3 dirHit)
        {
            AddForce(dirHit, velocity);
        }
        public override void OnWaveEnter(Vector3 dirHit, float power)
        {
            AddForce(dirHit, power);
        }
    }
}

