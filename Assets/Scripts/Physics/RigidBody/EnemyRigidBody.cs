﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace CustomPhysics
{
    public class EnemyRigidBody : BaseRigidBody
    {
        EnemyAnimManager animManager;
        delegate void CollInstr(Vector3 dir);
        CollInstr collInstr;
        Transform PlayerTransform;
        static int count = 0; 

        protected override void Start()
        {
            base.Start();
            count++;

            PlayerTransform = PlayerController.Instance.gameObject.transform; // cash player transform
            StartCoroutine(gravityCoroutine());
            animManager = GetComponent<EnemyAnimManager>();
            collInstr = new CollInstr(Fall);
            StartCoroutine(checkDistCoroutine());
        }
        void Fall(Vector3 hitDir)
        {
            animManager.Fall();
            collInstr = new CollInstr(Empty);
            StartCoroutine(AddHorForce(hitDir));
        }
        void Empty(Vector3 d) { }

        IEnumerator AddHorForce(Vector3 hitDir)
        {
            Vector3 dir = new Vector3(hitDir.x, 0);
            for(int i = 0;i<100;i++)
            {
                yield return new WaitForEndOfFrame();
                transform.position -= dir * Time.deltaTime;
            }
            count--;

            if (count == 0)
                PlayerController.Instance.Stop();
            this.enabled = false;
        }
        public override void CustomOnCollisionEnter(Vector3 hitDir)
        {
            collInstr(hitDir);
        }
        public override void OnWaveEnter(Vector3 hitDir, float pow)
        {
            collInstr(hitDir);
        }
  
        IEnumerator checkDistCoroutine()
        {
            yield return new WaitUntil(() => Vector3.Distance(transform.position, PlayerTransform.position) < UI.Instance.Dist);
            PlayerController.Instance.Shoot(transform.position);
            
        }
        #region Changestate
        public override void StartGravity()
        {
            gravity = 1;
        }
        public override void StopGravity()
        {
            gravity = 0;
        }
        #endregion Changestate
    }
}

