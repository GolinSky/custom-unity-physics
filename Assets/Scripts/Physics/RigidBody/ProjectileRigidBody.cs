﻿using System.Collections;
using UnityEngine;
using System;
namespace CustomPhysics
{
    public class ProjectileRigidBody : BaseRigidBody
    {
        delegate void Instr();
        Instr instr;
        float currAngle;
        Vector3 currTarg;
        float currDist;
        Coroutine cor;

        #region CollEvent
        void FirstColl()
        {
            instr = new Instr(() => { });
            Recalculate(SecondColl);
        }
        void SecondColl()
        {
            instr = new Instr(() => { });
            Recalculate(Explode);
        }
        void Explode()
        {
            gameObject.SetActive(false);
        }
        void Recalculate(Action method)
        {
            StopCoroutine(cor);
            currTarg = transform.position + Vector3.right * currDist / 2;
            currDist = Vector3.Distance(transform.position, currTarg);
            cor = StartCoroutine(ParabolaCoroutine(currTarg, currAngle, method));
            PoolManager.Instance.CreateExp(transform.position);
            PoolManager.Instance.CreateWave(transform.position).InitWave(UI.Instance.R);

        }
        #endregion CollEvent
        private void OnEnable()
        {
            instr = new Instr(FirstColl);
            StartCoroutine(gravityCoroutine());
        }
        private void OnDisable()
        {
            PoolManager.Instance.CreateExp(transform.position);
            PoolManager.Instance.CreateWave(transform.position).InitWave(UI.Instance.R);
        }
        IEnumerator ParabolaCoroutine(Vector3 targetPos, float angle)
        {
            gravity = 0;

            Vector3 startPos = transform.position;
            
            angle *= Mathf.Deg2Rad;
            float speed = Mathf.Sqrt((currDist * Force) / Mathf.Sin(2 * angle));
            float t = speed * Mathf.Sin(angle) / Force;// time 
            float h = (Mathf.Pow(speed, 2) * ((1 - Mathf.Cos(2 * angle)) / 2.0f)) / (2.0f * 9.81f);
            float dt = 0;

            while (dt < 5f)
            {
                yield return new WaitForFixedUpdate();
                dt += Time.fixedDeltaTime;
                transform.position = MathParabola.Parabola(startPos, targetPos, h, dt / 5f);
            }
            gravity = 1;
        }
        IEnumerator ParabolaCoroutine(Vector3 targetPos, float angle, Action method)
        {
            yield return ParabolaCoroutine(targetPos, angle);
            instr = new Instr(method);
        }

        #region changeState
        public override void StopGravity()
        {
            instr();
        }

        #endregion changeState
        public override void AddForce( Vector3 targetPos, float angle)
        {
            currTarg = targetPos;
            currAngle = angle;
            currDist = Vector3.Distance(targetPos, transform.position);
            cor = StartCoroutine(ParabolaCoroutine(targetPos, angle));
        }
        #region OrdinataResolvers
        bool NegativeQR(float angle)
        {
            return angle * Mathf.Rad2Deg < -180;
        }
        bool PositiveQR(float angle)
        {
            return angle * Mathf.Rad2Deg > -360;
        }
        #endregion OrdinataResolvers
    }
}

#region important
/* Vector3 dir = Vector3.zero;
            float t = 0;
            Vector3 targetPos = transform.position + Vector3.right * dist;
            Debug.Log("dist = " + ((speed*speed)*Mathf.Sin(2*angle))/9.8f +" --> "+dist);
            Vector3 startPos = transform.position;
            speed = 2*( Mathf.Sin(angle) * dist)/9.8f;
            Debug.Log("new speed = " + speed);
            float time = 2 *( (speed * Mathf.Sin(angle)) / 9.8f);
            //float t = dist / speed;
            Vector3 target = transform.position + Vector3.right * dist;
            while (dir.x <dist)
            {
                yield return new WaitForEndOfFrame();
                time -= Time.deltaTime;
                t += Time.deltaTime;
                dir.x = speed * t * Mathf.Cos(angle) * Time.deltaTime;
                dir.y = speed * t * Mathf.Sin(angle) * Time.deltaTime - 9.81f *( t * t )/2* Time.deltaTime;
                transform.localPosition += dir;
               
            }
            Debug.Log(" real dist = " + Vector3.Distance(transform.position, startPos));*/
#endregion important
