﻿using System.Collections;
using UnityEngine;
namespace CustomPhysics
{
    public class RigidBodyWave : BaseRigidBody
    {

        float radius;
        public float R { get { return radius; } private set { } }
   
        protected override void Start()
        {
            CustomPhysicsManager.Instance.AddWave(this);
        }
        public void InitWave(float radius)
        {
            this.radius = radius;

            StartCoroutine(IncreaseCoroutine());
        }
        IEnumerator IncreaseCoroutine()
        {
            int len = (int)radius + 1;
            for(float i = 2;i < len; i += 0.5f)
            {
                yield return new WaitForFixedUpdate();
                transform.localScale = Vector3.one * i;
            }
            gameObject.SetActive(false);
        }
        private void OnDisable()
        {
            transform.localScale = Vector3.one;
            StopAllCoroutines();
        }
        private void Update()
        {
            Vector3 pos = transform.position;
            Render.bounds.center.Set(pos.x, pos.y, pos.z);
        }
    }
}
