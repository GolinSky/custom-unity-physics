﻿using UnityEngine;
using UnityEngine.UI;

namespace CustomPhysics
{
    public class UI : MonoBehaviour
    {
        public static UI Instance;

        public float R { get; private set; }
        public float Dist { get; private set; }
        Button restartButt;
        private void Awake()
        {
            Instance = this;
           
        }
        private void Start()
        {
            InitSlider("angleSlider", 1, 89, 45, PlayerController.Instance.SetAngle);
            InitSlider("radiusSlider", 10, 50, 15, SetRadius);
            InitSlider("distanceSlider", 10, 40, 15, SetDistance);
            InitSlider("speedSlider", 1, 10, 2, PlayerController.Instance.SetSpeed);

            restartButt = transform.Find("restartButt").GetComponent<Button>();
            restartButt.onClick.AddListener(() =>
            {
                CustomPhysicsManager.Instance.Stop();
            });
        }
        Slider InitSlider(string path, int min, int max, int val, UnityEngine.Events.UnityAction<float> action)
        {
            Slider slider;
            slider = transform.Find(path).GetComponent<Slider>();
            slider.minValue = min;
            slider.maxValue = max;
            slider.value = val;
            slider.onValueChanged.AddListener(action);
            action(val);
            return slider;
        }
        void SetAngle(float angle)
        {
            PlayerController.Instance.SetAngle(angle);
        }
        void SetRadius(float radius)
        {
            R = radius;
        }
        void SetDistance(float dist)
        {
            Dist = dist;
        }
        /// <summary>
        /// END OF GAME  
        /// </summary>
        public void EOF()
        {
            restartButt.gameObject.SetActive(true);
        }
    }
}


